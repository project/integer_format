Integer Field Formatter
=======================

This module adds a widget and display formatter to the basic "integer" field to
edit and display it as a decimal field. It is generally considered a best
practice to store ecommerce prices as integers in the lowest denomination such
as cents. This avoids a whole class of rounding and other calculation errors.

There are other modules for Drupal that support "price" fields, but they all
have a limitation that makes them unsuitable for many use cases:

 * [Formatted Number](https://www.drupal.org/project/formatted_number) only
   supports formatting on it's custom field type, and isn't built on the
   integer field. To do decimal formatting you have to use the decimal field
   type, which uses MySQL's decimal() type in the database.
 * [Money](https://www.drupal.org/project/money) also stores it's data in MySQL
   as a decimal() field.
 * [Commerce price](http://cgit.drupalcode.org/commerce/tree/modules/price/commerce_price.module)
   stores data as a serialized blob within it's field, which prevents any
   custom MySQL queries on the data.
 * [Currency field](https://www.drupal.org/sandbox/ydnar79/1412044) is
   explicitly for currency data (USD, CAD, etc) and not prices. It might be
   useful in combination with this module.

Integer Field Formatter addresses all of these concerns by using a basic
integer field for storage, while modifying the display and editing
functionality in the front end.

Installation
------------

This module requires:

 * [Libraries](https://www.drupal.org/project/libraries)
 * The [autoNumeric](https://github.com/BobKnothe/autoNumeric/releases) jQuery
   plugin, version 1.x. Download and extract the library to
   ```sites/all/libraries/autoNumeric```.

Enable the Integer Format module, and then configure settings on integer fields
to add the formatter.
