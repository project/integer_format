(function($) {
  /**
   * Behaviors for formatting an integer widget as a decimal field.
   * @type {{attach: Function, bracketSetting: Function, formatFromInteger: Function, parseToInteger: Function}}
   */
  Drupal.behaviors.integerFormat = {

    /**
     * Attach callback to add autoNumeric() to a field.
     *
     * @param context
     *   The context of the attach call.
     * @param settings
     *   The settings to use when attaching, from Drupal.settings.
     */
    attach: function(context, settings) {
      for (var widget in settings.integerFormat) {
        // Handle skipping over defined but empty properties.
        if (!settings.integerFormat.hasOwnProperty(widget)) {
          continue;
        }

        // Default settings.
        // We know we are always binding to an integer field, which means that
        // if minimum and maximum are undefined that we can fall back on MySQL
        // ranges for the field.
        // https://dev.mysql.com/doc/refman/5.6/en/integer-types.html
        var widgetSettings = $.extend({
            scale: 0,
            min: -2147483648,
            max: 2147483647,
            negativeDisplay: null,
            prefix: ''
          },
          settings.integerFormat[widget]
        );

        var selector = '.' + widget;
        var $field = $(selector);

        // Parse the integer in the field into a decimal.
        if ($field.val() != '') {
          var formatted = this.formatFromInteger($field.val(), widgetSettings.scale);
          $field.once(widget + '-val').val(formatted);
        }

        // Parse an integer in HTML content into a decimal.
        if ($field.html() != '') {
          var formatted = this.formatFromInteger($field.html(), widgetSettings.scale);
          $field.once(widget + '-html').html(formatted);
        }

        // Attach autoNumeric functionality to the field.
        $field.once(widget + '-an').autoNumeric('init', {
          'vMin': widgetSettings.min,
          'vMax': widgetSettings.max,
          'mDec': widgetSettings.scale,
          'nBracket': this.bracketSetting(widgetSettings.negativeDisplay),
          'aSign': widgetSettings.prefix
        });

        // Handle converting a decimal back to an integer when submitting the
        // a form.
        $field.closest('form').once(widget).submit((function($field, widgetSettings, selector) {
          // noinspection JSUnusedLocalSymbols
          return (function(e) {
            var value = $field.autoNumeric('get');
            if (value == '') {
              return;
            }

            value = Drupal.behaviors.integerFormat.parseToInteger(value, widgetSettings.scale);

            // noinspection JSCheckFunctionSignatures
            $field.autoNumeric('destroy').removeOnce(selector);
            $field.val(value);
          });
        })($field, widgetSettings, selector));
      }
    },

    /**
     * Return the nBracket setting for use with autoNumeric based on the field
     * setting.
     *
     * @param {string} bracket
     *   The bracket setting to map, such as 'parentheses'.
     *
     * @returns {string}
     *   A setting suitable for autoNumeric's nBracket setting.
     */
    bracketSetting: function (bracket) {
      switch (bracket) {
        case 'parentheses':
          return '(,)';

        case 'brackets':
          return '[,]';

        case 'braces':
          return '{,}';

        case 'angle brackets':
          return '<,>';

        default:
        // 'dash' is default and corresponds to null.
      }
    },

    /**
     * Parse an integer value such as 12345 into 123.45, based on the scale.
     *
     * @param {int} value
     *   The integer to parse.
     * @param {int} scale
     *   The scale corresponding to the number of decimal places of the value.
     *
     * @returns {string}
     *   The formatted string of value.
     */
    formatFromInteger: function (value, scale) {
      var formatted = value.toString();
      if (value.length <= scale) {
        formatted = '0.' + new Array(scale - value.length + 1).join('0') + formatted;
      }
      else {
        // We need to split apart the value so we can insert the decimal place.
        formatted = formatted.substr(0, value.length - scale) + '.' + formatted.substr(value.length - scale);
      }
      return formatted;
    },

    /**
     * Parse a decimal string as an integer, such as converting 12.34 into 1234.
     *
     * @param {string} formatted
     *   The string to return as an integer.
     * @param {int} scale
     *   The scale of the decimal formatted string.
     *
     * @returns {int}
     *   Returns an integer parsed from the formatted string.
     */
    parseToInteger: function (formatted, scale) {
      if (formatted == '') {
        return 0;
      }

      // If we have fractional values, we can just remove the decimal
      // place.
      var value = 0;
      var decimal = formatted.indexOf('.');
      if (decimal != -1) {
        // In the case of trailing zeros, we need to append scale - 1.
        // This changes 0.1 into 0.01.
        if (formatted.substr(decimal + 1).length < scale) {
          formatted = formatted + new Array(scale - formatted.substr(decimal + 1).length + 1).join('0');
        }
        value = parseInt(formatted.replace('.', ''), 10);
      }
      else {
        // We don't have a fractional value, so we need to append zeros.
        value = formatted + new Array(scale + 1).join('0');
      }

      return value;
    }
  }
})(jQuery);
