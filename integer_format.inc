<?php

/**
 * @file
 * Form and render functions for integer formatting.
 */

/**
 * Form callback for the decimal format widget.
 */
function integer_format_field_widget_integer_format_decimal_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $wrapper = number_field_widget_form($form, $form_state, $field, $instance, $langcode, $items, $delta, $element);
  $widget_class = drupal_html_class($instance['field_name'] . '-' . $instance['id']);
  _integer_format_attach($wrapper['value'], $widget_class, $instance['widget']['settings']);

  return $wrapper;
}

/**
 * Return the settings form for integer formatting.
 *
 * @param array $settings
 *   Array of the default settings to use for the form.
 *
 * @return array
 *   A Form API array.
 */
function integer_format_settings_form($settings) {
  $form = array();
  $form['scale'] = array(
    '#type' => 'textfield',
    '#title' => t('Scale'),
    '#description' => t('Enter the number of decimal places to use in the field widget.'),
    '#default_value' => $settings['scale'],
    '#element_validate' => array('integer_format_widget_scale_validate'),
    '#required' => TRUE,
  );

  $form['negative_display'] = array(
    '#type' => 'select',
    '#title' => t('Display negative numbers as'),
    '#default_value' => $settings['negative_display'],
    '#options' => array(
      'dash' => t('Dash'),
      'parentheses' => t('(parentheses)'),
      'brackets' => t('[brackets]'),
      'braces' => t('{braces}'),
      'angle brackets' => t('<angle brackets>'),
    ),
  );

  $form['prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Numeric prefix'),
    '#description' => t('Specify a prefix to be used within the text field.'),
    '#default_value' => $settings['prefix'],
    '#element_validate' => array('integer_format_widget_prefix_validate'),
  );

  return $form;
}

/**
 * Validate that scale for decimal formatting is never negative.
 */
function integer_format_widget_scale_validate($element, &$form_state, $form) {
  if ($element['#value'] < 0) {
    form_error($element, t('Scale must not be negative.'));
  }
}

/**
 * Validate the prefix value is allowed by autoNumeric.
 */
function integer_format_widget_prefix_validate($element, &$form_state, $form) {
  if (preg_match("!.*[0-9,'].*!", $element['#value'])) {
    form_error($element, t('Prefix can not be numeric or contain numbers.'));
  }
}

/**
 * Attach autonumeric behaviour to a form or renderable element.
 *
 * @param array $element
 *   The element to attach autonumeric behaviour to. This will overwrite
 *   #attached and #attributes on $element.
 * @param string $widget_class
 *   The class to use for the element.
 * @param array $settings
 *   The settings of the widget.
 */
function _integer_format_attach(&$element, $widget_class, $settings) {
  $element['#attached'] = array(
    'libraries_load' => array(
      array('autoNumeric'),
    ),
    'js' => array(
      drupal_get_path('module', 'integer_format') . '/integer-format.js',
      array(
        'type' => 'setting',
        'data' => array(
          'integerFormat' => array(
            $widget_class => array(
              'scale' => (int) $settings['scale'],
              'negativeDisplay' => $settings['negative_display'],
              'prefix' => $settings['prefix'],
            ) + array_filter($settings),
          ),
        ),
      ),
    ),
  );

  $element['#attributes'] = array('class' => array('auto-numeric', $widget_class));
}
